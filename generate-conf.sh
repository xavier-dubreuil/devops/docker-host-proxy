#!/bin/bash

CFG_FILE="/usr/local/etc/haproxy/haproxy.cfg"

OLD_IFS=${IFS}
IFS=$'\n'

append_cfg() {
  echo "$1" >>"${CFG_FILE}"
}

generique() {
  append_cfg "$1"
  for ENV_KEY in $(env | grep -Ei "^$1_" | sort | cut -d"=" -f1 | uniq); do
    NAME=$(echo "${ENV_KEY}" | cut -d"_" -f2-)
    VALUE=$(eval "echo \$${ENV_KEY}")
    append_cfg "    ${NAME} ${VALUE}"
  done
  append_cfg ""
}

endpoints() {
  for TYPE_KEY in $(env | grep -Ei "^(FRONTEND|BACKEND)_" | sort | cut -d"_" -f1-2 | uniq); do
    TYPE=$(echo "${TYPE_KEY}" | cut -d"_" -f1 | tr '[:upper:]' '[:lower:]')
    NAME=$(echo "${TYPE_KEY}" | cut -d"_" -f2 | tr '[:upper:]' '[:lower:]')
    append_cfg "${TYPE} ${NAME}"
    for ENV_KEY in $(env | grep -i "${TYPE}_${NAME}_" | sort | cut -d"=" -f1 | uniq); do
      PARAM=$(echo "${ENV_KEY}" | cut -d"_" -f3- | tr '[:upper:]' '[:lower:]')
      # Getting Query
      VALUE=$(eval "echo \$${ENV_KEY}")
      # Continuing if the query is empty
      append_cfg "    ${PARAM} ${VALUE}"
    done
    append_cfg ""
  done
}

generique global
generique defaults
endpoints

IFS=${OLD_IFS}

cat $CFG_FILE
