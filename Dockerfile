FROM haproxy:2.3

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

COPY generate-conf.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/generate-conf.sh
